# RESTful CRUD
## предназначение
реализация RESTful CRUD на яп GO с использованием Postgre в качестве ядка бд
## задача
Модель - Person
Поля модели: Id, email, phone, firstName, lastName БД - sqlite 

Http-методы:
GET/person/ - возвращает список моделей Person о которых у нас естьинфа. 
(параметры limit, offset, search будут плюсом)
GET/person/{id} – возвращает одну модель Person. 
POST/person/ – создаёт модель Person
PUT/person/{id} – обновляет модель Person 
DELETE /person/{id} – удаляет модель Person 

Все данные передавать и выводить в json формате.

