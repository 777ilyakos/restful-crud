package repository

import (
	dbr "github.com/gocraft/dbr/v2"
	_ "github.com/lib/pq"
	"github.com/sirupsen/logrus"
)

func CreateDbrConn() *dbr.Connection {
	conn, err := dbr.Open("postgres", "host=localhost port=5432 user=postgres password=123456 dbname=db_person sslmode=disable", nil)
	if err != nil {
		logrus.Fatalf("failed to open: %s", err)
	}
	return conn
}
