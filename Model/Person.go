package ModelPerson

type Person struct {
	Id        int    `json:"Id" db:"Id"`
	Email     string `json:"Email" db:"Email"`
	Phone     string `json:"Phone" db:"Phone"`
	FirstName string `json:"FirstName" db:"FirstName"`
	LastName  string `json:"LastName" db:"LastName"`
}
