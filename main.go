package main

import (
	// "fmt"
	"errors"
	"net/http"
	"os"
	model "restful-crud/Model"
	Repository "restful-crud/Repository"
	"strconv"

	echo "github.com/labstack/echo/v4"

	_ "github.com/lib/pq"
	"github.com/sirupsen/logrus"
)

func main() {

	var log = logrus.New()
	log.Formatter = new(logrus.TextFormatter)
	log.Formatter.(*logrus.TextFormatter).DisableColors = false
	log.Formatter.(*logrus.TextFormatter).DisableTimestamp = false

	file, err := os.OpenFile("logrus.log", os.O_CREATE|os.O_WRONLY, 0666)
	if err == nil {
		log.Out = file
	} else {
		log.Info("Failed to log to file, using default stderr")
	}

	conn := Repository.CreateDbrConn()
	// ограничение количества запросов к субд
	conn.SetMaxOpenConns(10)
	sess := conn.NewSession(nil)

	// create a tx from sessions
	sess.Begin()
	// sess.Close()

	e := echo.New()
	e.GET("/GET/person/", func(c echo.Context) error {
		var persons []model.Person
		var err error
		var count int
		if c.FormValue("Id") == "" {
			count, err = sess.Select("*").From("public.\"Person\"").Load(&persons)
			if err == nil {
				log.WithFields(logrus.Fields{"CountRecords": count}).Info("Request GET/person/")
				return c.JSON(http.StatusOK, persons)
			} else {
				log.WithFields(logrus.Fields{"err_message": err}).Error("Request GET/person/")
				return c.JSON(400, err)
			}
		} else {
			count, err = sess.Select("*").From("public.\"Person\"").Where("\"Id\"=" + c.FormValue("Id")).Load(&persons)
			if count == 0 {
				err = errors.New("Record Not Found")
			}
			if err == nil {
				log.WithFields(logrus.Fields{"CountRecords": count, "Id": c.FormValue("Id")}).Info("Request GET/person/{id}")
				return c.JSON(http.StatusOK, persons[len(persons)-1])
			} else {
				log.WithFields(logrus.Fields{"Id": c.FormValue("Id"), "err_message": err}).Error("Request GET/person/{id}")
				return c.JSON(400, err.Error())
			}
		}
	})
	e.POST("POST/person/", func(c echo.Context) error {
		var newPeson model.Person
		var err error
		newPeson.Email = c.FormValue("email")
		newPeson.FirstName = c.FormValue("firstName")
		newPeson.LastName = c.FormValue("lastName")
		newPeson.Phone = c.FormValue("phone")
		_, err = sess.InsertBySql("INSERT INTO public.\"Person\" (\"Email\",\"Phone\",\"LastName\",\"FirstName\") VALUES ('" + newPeson.Email + "','" + newPeson.Phone + "','" + newPeson.LastName + "','" + newPeson.FirstName + "')").Exec()
		if err == nil {
			log.WithFields(logrus.Fields{
				"Email":     c.FormValue("Email"),
				"Fhone":     c.FormValue("Phone"),
				"FirstName": c.FormValue("FirstName"),
				"LastName":  c.FormValue("LastName")}).Info("POST/person/")
			return c.JSON(http.StatusCreated, newPeson)
		} else {
			log.WithFields(logrus.Fields{
				"Email":       c.FormValue("Email"),
				"Fhone":       c.FormValue("Phone"),
				"FirstName":   c.FormValue("FirstName"),
				"LastName":    c.FormValue("LastName"),
				"err_message": err}).Error("POST/person/")
			return c.JSON(400, err.Error())
		}
	})
	e.PUT("PUT/person/", func(c echo.Context) error {
		var refPeson model.Person
		var err error
		refPeson.Email = c.FormValue("email")
		refPeson.FirstName = c.FormValue("firstName")
		refPeson.LastName = c.FormValue("lastName")
		refPeson.Phone = c.FormValue("phone")
		refPeson.Id, err = strconv.Atoi(c.FormValue("Id"))
		if err != nil {
			log.WithFields(logrus.Fields{"Id": c.FormValue("Id"), "err_message": err}).Error("PUT/person/")
			return c.JSON(400, err.Error())
		}
		_, err = sess.UpdateBySql("UPDATE public.\"Person\" SET \"Email\"=" + refPeson.Email + ", \"Phone\"=" + refPeson.Email + ", \"LastName\"=" + refPeson.Email + ", \"FirstName\"=" + refPeson.Email + " WHERE \"Id\"=" + c.FormValue("Id") + ";").Exec()
		if err == nil {
			log.WithFields(logrus.Fields{
				"Id":        c.FormValue("Id"),
				"Email":     c.FormValue("Email"),
				"Fhone":     c.FormValue("Phone"),
				"FirstName": c.FormValue("FirstName"),
				"LastName":  c.FormValue("LastName")}).Info("POST/person/")
			return c.JSON(http.StatusUpgradeRequired, refPeson)
		} else {
			log.WithFields(logrus.Fields{
				"Id":          c.FormValue("Id"),
				"Email":       c.FormValue("Email"),
				"Fhone":       c.FormValue("Phone"),
				"FirstName":   c.FormValue("FirstName"),
				"LastName":    c.FormValue("LastName"),
				"err_message": err}).Error("POST/person/")
			return c.JSON(400, err.Error())
		}
	})
	e.DELETE("DELETE/person/", func(c echo.Context) error {
		var Id int
		var err error
		Id, err = strconv.Atoi(c.FormValue("Id"))
		if err != nil {
			log.WithFields(logrus.Fields{"Id": c.FormValue("Id"), "err_message": err}).Error("DELETE/person/")
			return c.JSON(400, err.Error())
		}
		_, err = sess.DeleteBySql("DELETE FROM public.\"Person\" WHERE \"Id\"=" + c.FormValue("Id") + ";").Exec()
		if err == nil {
			log.WithFields(logrus.Fields{"Id": c.FormValue("Id")}).Info("Request GET/person/{id}")
			return c.JSON(http.StatusOK, Id)
		} else {
			log.WithFields(logrus.Fields{"Id": c.FormValue("Id"), "err_message": err}).Error("Request GET/person/{id}")
			return c.JSON(400, err.Error())
		}
	})
	e.Logger.Fatal(e.Start(":1323"))
}
